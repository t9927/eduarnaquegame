<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class GamesController extends AbstractController
{
    public function __construct(
        private HttpClientInterface $client
    )
    {
    }
    
    #[Route('/', name: 'app_games')]
    public function index(): Response
    {
        $posts = $this->getPosts();

        return $this->render('games/index.html.twig', [
            'posts' => $posts,
        ]);
    }
    #[Route('/{id}', name: 'app_games_post')]
    public function show(int $id): Response
    {
        $post = $this->getPost($id);
        return $this->render('games/show.html.twig', [
            'post' => $post,
        ]);
    }

    private function getPosts(): array
    {
        $reponse = $this->client->request(
            'GET',
            'https://env-5261249.hidora.com/posts'
        );

        return $reponse->toArray();

    }

    private function getPost(int $id): array
    {
        $reponse = $this->client->request(
            'GET',
            'https://env-5261249.hidora.com/posts/'.$id
        );

        return $reponse->toArray();

    }

}
